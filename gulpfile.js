/**
 * 汎用Gulpfile
 *
 * @author    Seiji Ogawa [@ogaaaan]
 * @license   MIT license
 * @copyright ogaaaan
 */

/**
 * モジュール読み込み
 *
 */

// base
var gulp =       require('gulp');             // わしじゃよ
var notify =     require('gulp-notify');      // デスクトップ通知
var plumber =    require("gulp-plumber");     // エラー時に処理を停止させない
var rename =     require('gulp-rename');      // ファイル名変更
var useref =     require('gulp-useref');      // ファイルパス変換
var shell =      require('gulp-shell');       // シェル実行
var del =        require('del');              // ファイルの削除
var replace =    require('gulp-replace');     // 置換
var watch =      require('gulp-watch');       // 新規ファイル監視

// html
var ejs =        require('gulp-ejs');         // ejsテンプレートコンパイル
var jade =       require('gulp-jade');        // jadeテンプレートコンパイル
var htmlhint =   require('gulp-htmlhint');    // html文法チェック

// css
var compass =    require('gulp-compass');     // compass+sass
var less =       require('gulp-less');        // less
var csslint =    require('gulp-csslint');     // css構文チェック
var cssmin =     require('gulp-cssmin');      // css最適化

// js
var coffee =     require('gulp-coffee');      // coffee script
var typescript = require('gulp-typescript');  // type script
var jshint =     require('gulp-jshint');      // js構文チェック
var uglify =     require('gulp-uglify');      // js最適化

/**
 * 初期設定
 *
 */
var config = {
    // クリーンアップ対象
    clean: [
        'dist/*',
        'tmp/*',
        '!dist/.gitkeep',
        '!tmp/.gitkeep',
    ],
    // ejsコンパイル設定
    ejs: {
        src: [
            'src/ejs/**/*.ejs',
            '!src/ejs/**/_*.ejs'
        ],
        dest: 'src/html/',
    },
    // jadeコンパイル設定
    jade: {
        src: [
            'src/jade/**/*.jade',
            '!src/jade/**/_*.jade'
        ],
        dest:'src/html/',
    },
    // html hint設定
    htmlhint: {
        src: 'src/html/**/*.html'
    },
    // sassコンパイル設定
    sass: {
        src: [
            'src/sass/**/*.scss',
            '!src/sass/**/_*.scss'
        ],
        dest: 'src/css/',
        compass: {
            sass: 'src/sass/',
            css: 'src/css/',
            style: 'expanded'
        }
    },
    // lessコンパイル設定
    less: {
        src: [
            'src/less/**/*.less',
            '!src/sass/**/_*.less'
        ],
        dest: 'src/css/',
    },
    // css lint設定
    csslint: {
        src: [
            'src/css/**/*.css',
            '!src/css/**/*.min.css',
            '!src/css/**/*-min.css',
            '!src/css/**/*_min.css'
        ]
    },
    // coffeeコンパイル設定
    coffee: {
        src: [
            'src/coffee/**/*.coffee',
            '!src/coffee/**/_*.coffee'
        ],
        dest: 'src/js/'
    },
    // typescriptコンパイル設定
    typescript: {
        src: [
            'src/ts/**/*.ts',
            '!src/ts/**/_*.ts'
        ],
        dest: 'src/js/'
    },
    // js hint設定
    jshint: {
        src: [
            'src/js/**/*.js',
            '!src/js/**/*.min.js',
            '!src/js/**/*-min.js',
            '!src/js/**/*_min.js'
        ],
    },
    // src内をtmpにコピーする設定
    src_tmp: {
        src: [
            'src/js/**/*',
            'src/css/**/*',
            'src/img/**/*',
            'src/fonts/**/*'
        ],
        dest: 'tmp/'
    },
    // html以外のファイルの中身を任意に書き換える設定
    replaceto: {
        src: [
            'tmp/js/**/*.js',
            'tmp/css/**/*.css',
            '!tmp/js/**/*.min.js',
            '!tmp/js/**/*-min.js',
            '!tmp/js/**/*_min.js',
            '!tmp/css/**/*.min.css',
            '!tmp/css/**/*-min.css',
            '!tmp/css/**/*_min.css'
        ],
        dest: 'tmp/',
    },
    // cssを最適化する設定
    css_minify: {
        src: [
            'tmp/css/**/*.css',
            '!tmp/css/**/*.min.css',
            '!tmp/css/**/*-min.css',
            '!tmp/css/**/*_min.css'
        ],
        dest: 'tmp/css/',
    },
    // jsを難読化する設定
    js_uglify: {
        src: [
            'tmp/js/**/*.js',
            '!tmp/js/**/*.min.js',
            '!tmp/js/**/*-min.js',
            '!tmp/js/**/*_min.js'
        ],
        dest: 'tmp/js/',
    },
    // tmp内をdistにコピーする設定
    tmp_dist: {
        src: [
            'tmp/js/**/*.min.js',
            'tmp/js/**/*.json',
            'tmp/css/**/*.min.css',
            'tmp/img/**/*',
            'tmp/fonts/**/*'
        ],
        dest: 'dist'
    },
    // htmlファイルをdist直下にコピーする設定
    html_dist: {
        src: [
            'src/html/**/*.html'
        ],
        dest: 'dist/'
    },
    // html内のパスをリリース用に書き換える設定
    html_replace: {
        src: [
            'dist/**/*.html'
        ],
        dest: 'dist'
    },
    test_replace: {
        src: [
            'src/test/**/*.js',
            '!src/test/dest'
        ],
        dest: 'src/test/dest'
    }
};

/**
 * 各種タスク設定
 *
 */
gulp.task('test_replace', function () {
    var conf = config.test_replace;
    return gulp.src(conf.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(replace(/(  +).+ replaceto: */g, '$1'))
        .pipe(gulp.dest(conf.dest));
});

gulp.task('listen', function () {
    watch(config.ejs.src[0], function () {
        return gulp.start('ejs');
    });
});

// デフォルト（ウォッチ）
gulp.task('default', function () {
    gulp.start('watch');
});

// ウォッチ
gulp.task('watch', ['today'], function () {
    gulp.watch(config.ejs.src[0],       ['ejs']);
    gulp.watch(config.jade.src[0],      ['jade']);
    gulp.watch(config.htmlhint.src,     ['htmlhint']);
    gulp.watch(config.sass.src[0],      ['sass']);
    gulp.watch(config.less.src[0],      ['less']);
    gulp.watch(config.csslint.src,      ['csslint']);
    gulp.watch(config.coffee.src,       ['coffee']);
    gulp.watch(config.typescript.src,   ['typescript']);
    gulp.watch(config.jshint.src,       ['jshint']);
});

// 定期表示（コンソールでの進捗を改善。毎回違う文字列を一定間隔で表示）
gulp.task('today', function () {
    setInterval(function () {
        console.log(
            " ------ [%s]" +
            " ------ [%s]",
            new Date(),
            +new Date()
        );
    }, 15000);
});


// リリース
gulp.task('release', [
    'clean',
    'ejs',
    'jade',
    'sass',
    'less',
    'coffee',
    'typescript',
    'src_tmp',
    'replaceto',
    'css_minify',
    'js_uglify',
    'tmp_dist',
    'html_dist',
    'html_replace'
], function (config) {
    console.log(' - DONE -');
});

// クリーンアップ
gulp.task('clean', ['ejs', 'jade', 'sass', 'less', 'coffee'], function () {
    var conf = config.clean;
    del(conf);
});

// ejsコンパイル
gulp.task('ejs', function () {
    var conf = config.ejs;
    return gulp.src(conf.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(ejs())
        .pipe(gulp.dest(conf.dest));
});

// jadeコンパイル
gulp.task('jade', function () {
    var conf = config.jade;
    return gulp.src(conf.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest(conf.dest));
});

// html構文チェック
gulp.task('htmlhint', ['ejs', 'jade'], function () {
    var conf = config.htmlhint;
    return gulp.src(conf.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(htmlhint())
        .pipe(htmlhint.reporter());
});

// sassコンパイル（compassの設定ファイル`config.rb`を読み込む）
gulp.task('sass', function () {
    var conf = config.sass;
    return gulp.src(conf.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(compass({
            config_file: './config.rb',
            sass: conf.compass.sass,
            css: conf.compass.css,
            style: conf.compass.style
        }))
        .pipe(gulp.dest(conf.dest));
});

// lessコンパイル
gulp.task('less', function () {
    var conf = config.less;
    return gulp.src(conf.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(less())
        .pipe(gulp.dest(conf.dest));
});

// css構文チェック
gulp.task('csslint', function () {
    var conf = config.csslint;
    return gulp.src(conf.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(csslint())
        .pipe(csslint.reporter());
});

// coffeeコンパイル
gulp.task('coffee', function () {
    var conf = config.coffee;
    return gulp.src(conf.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(coffee())
        .pipe(gulp.dest(conf.dest));
});

// typescriptコンパイル
gulp.task('typescript', function () {
    var conf = config.typescript;
    return gulp.src(conf.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(typescript({
            target: 'ES5',
            declaration: true
        }))
        .pipe(gulp.dest(conf.dest));
});

// js構文チェック
gulp.task('jshint', function () {
    var conf = config.jshint;
    return gulp.src(conf.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(jshint())
        .pipe(jshint.reporter());
});

// src内のhtml以外をtmpにコピーする
gulp.task('src_tmp', ['clean'], function () {
    var conf = config.src_tmp;
    return gulp.src(conf.src, { base: 'src' })
        // .pipe(plumber({
        //     errorHandler: notify.onError('<%= error.message %>')
        // }))
        .pipe(gulp.dest(conf.dest));
});

// ファイル内のreplaseto以降の文字列に入れ替える
gulp.task('replaceto', ['src_tmp'], function () {
    var conf = config.replaceto;
    var cmd = ['sed -i -r -e "s/^.+replaceto://g" <%= file.path %>'];
    return gulp.src(conf.src, { base: 'tmp/' })
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(shell(cmd));
});

// css最適化
gulp.task('css_minify', ['replaceto'], function () {
    var conf = config.css_minify;
    return gulp.src(conf.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(cssmin())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(conf.dest));
});

// js最適化
gulp.task('js_uglify', ['css_minify'], function () {
    var conf = config.js_uglify;
    return gulp.src(conf.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(conf.dest));
});

// リリース用にファイルをコピー
gulp.task('tmp_dist', ['css_minify', 'js_uglify'], function() {
    var conf = config.tmp_dist;
    return gulp.src(conf.src, { base: 'tmp' }
        )
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(gulp.dest(conf.dest));
});

// htmlをdist直下にコピーする
gulp.task('html_dist', ['tmp_dist'], function () {
    var conf = config.html_dist;
    return gulp.src(conf.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(gulp.dest(conf.dest));
});

// html内のファイルパスをリリース用に書き換える
gulp.task('html_replace', ['html_dist'], function () {
    var conf = config.html_replace;
    return gulp.src(conf.src)
        .pipe(plumber({
            errorHandler: notify.onError('<%= error.message %>')
        }))
        .pipe(useref({ searchPath: '/' }))
        .pipe(gulp.dest(conf.dest));
});
