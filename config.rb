# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "./src"
css_dir = "./src/css"
sass_dir = "./src/sass"
images_dir = "./src/img"
javascripts_dir = "./src/js"
# fonts_dir = "./src/fonts"

# コメント出力
line_comments =false
# 相対パス
relative_assets = true
# 記述フォーマット
preferred_syntax = :scss
# 出力フォーマット
output_style = :expanded

# スプライト画像のハッシュ値無しバージョンも生成する
on_sprite_saved do |filename|
  if File.exists?(filename)
    FileUtils.cp filename, filename.gsub(%r{-s[a-z0-9]{10}\.png$}, '.png')
  end
end
on_stylesheet_saved do |filename|
  if File.exists?(filename)
    css = File.read filename
    File.open(filename, 'w+') do |f|
      f << css.gsub(%r{-s[a-z0-9]{10}\.png}, '.png')
    end
  end
end
