/**
 * AIDEM SFA JavaScript
 *
 * @fileoverview require files are below.
 * <ul>
 *     <li>jquery</li>
 *     <li>underscorejs</li>
 *     <li>requirejs</li>
 * </ul>
 *
 * @author    Seiji Ogawa [s_ogawa@aidem.co.jp]
 * @copyright 2016.02 aidem, ink.
 *
 */
require([
    'app',  // replaceto: 'app.min',
    'validation' // replaceto: 'validation.min'
], function (App, Valid) {

    'use strict';

    var ua = window.navigator.userAgent;

    /**
     * msg
     *
     * リテラルなメッセージ
     *
     * @type {Object}
     */
    var msg = {
        confirm: {
            reset: 'この行をリセットしてもよろしいですか？',
            remove: 'この行を削除してもよろしいですか？'
        },
        string: {
            area: 'エリア'
        }
    };

    /**
     * アプリケーション起動
     *
     */
    App.init().done(function (self) {

        /**
         * フォームの変更検出
         *
         */
        $('textarea, input, select').on('change', function () {

            var self = this;

            // サブミットボタンを有効にする
            $('.js-submit').removeAttr('disabled');

            // ヒント表示
            App.setHint(self);

            // バリデーション
            var isValid = Valid.validate();

            // バリッド時のみ実行
            if (isValid) {
                // 見積もりリアルタイム変更
                App.setAggregateAll();
            }

        });


        /**
         * プラスボタン
         *
         * タップで最終行に1行追加
         */
        $('.js-sfa-append').on('click', function () {

            // オブジェクトのキャッシュ生成
            var $parent = $(this).closest('.sfa-article'),
                $child = $(this).parent().prev('.js-sfa-form'),
                $clone = $child.clone(true);

            // 複製のフォーム内容をリセット
            App.setFormElementsToBlank($clone);

            // 複製をボタンの上に追加
            $(this).parent().before($clone);

            // name属性の再構築
            App.setNamesValueToHash($parent);

            return false;

        });

        /**
         * マイナスボタン
         *
         * タップで自分自身の行要素をすべて削除
         * 最終行のみにプラスボタンを表示する。
         */
        $('.js-sfa-btn-remove').on('click', function () {

            // オブジェクトのキャッシュ生成
            var $parent = $(this).closest('.sfa-article'),
                $child  = $(this).closest('.js-sfa-form');

            // 子が1つの場合、削除ボタンはリセットボタンとする
            var len = $parent.children().not('.sfa-ignore').length;

            if (len === 1) {
                // 同フィールドのフォームをリセット
                if (confirm(msg.confirm.reset)) {
                    App.setFormElementsToBlank($child);
                }
            } else {
                // 直前の子を削除
                if (confirm(msg.confirm.remove)) {
                    $child.remove();
                }
            }

            // 最後の子の追加ボタンを表示
            var $last = $parent.children().last();
            $('.sfa-btn-append', $last).css('visibility', 'visible');

            // name属性の再構築
            App.setNamesValueToHash($parent);

            // フォーム内容を集計
            App.setAggregateAll();

            return false;
        });

        /**
         * アラート非表示
         *
         * タップすると閉じるようにする
         */
        $('.js-sfa-alert, .js-sfa-information').on('click', function () {
            $(this).css({
                display: 'none'
            });
        });

        /**
         * サブミットボタン
         *
         * 変更がないときはDisabled
         */
        $('.js-submit').on('click', function () {
            if(Valid.beforeSave()) {

                // ローカルストレージに保存
                var data = App.getSaveData();

                //$(this).closest('form').submit();
            }
        });

    });

});
