/**
 * AIDEM SFA App
 *
 * @fileoverview require files are below.
 * <ul>
 *     <li>jquery</li>
 *     <li>underscorejs</li>
 *     <li>requirejs</li>
 * </ul>
 *
 * @author    Seiji Ogawa [s_ogawa@aidem.co.jp]
 * @copyright 2016.02 aidem, ink.
 *
 */

/**
 * アプリケーションオブジェクト
 *
 * @type    {Object}
 * @return  {Object} app
 */
define(function () {

    'use strict';

    var app = {
        /**
         * インタフェース用JSONファイル
         *
         * @type {Object}
         *
         */
        interfaceFile: '../../js/interface.json', // replaceto:interfaceFile: '../js/interface.json',
        /**
         * interface インタフェース用jsonデータ
         * @type {Object}
         */
        interface: {},
        /**
         * aggregate
         *
         * 集計用の集計データ
         *
         * <pre>
         * [
         *     ['商材名 プラン名 期間名', '個数', '単価'],
         *     ['商材名 プラン名 期間名', '個数', '単価'],
         *     ['商材名 プラン名 期間名', '個数', '単価']
         * ]
         * </pre>
         *
         * @type {Array}
         */
        aggregate: [],
        /**
         * discount description
         *
         * 割引用の集計データ
         *
         * <pre>
         * [
         *     ['商材名 割引名', '割引額'],
         *     ['商材名 割引名', '割引額'],
         *     ['商材名 割引名', '割引額']
         * ]
         * </pre>
         *
         * @type {Array}
         */
        discount: [],
        /**
         * estimate
         *
         * aggregateを基にした見積もりデータ
         *
         * <pre>
         * {
         *     "見積もり": {
         *         ['商材名 期間', '個数', '金額'],
         *         ['商材名 期間', '個数', '金額'],
         *         ['商材名 期間', '個数', '金額']
         *     },
         *     "割引": {
         *         ['キャンペーン名', '金額'],
         *         ['キャンペーン名', '金額'],
         *         ['キャンペーン名', '金額']
         *     }
         * ]
         * </pre>
         *
         * @type {Object}
         */
        estimate: {},
        /**
         * savedata
         *
         * @type {Object}
         */
        savedata: {},
        /**
         * msg
         *
         * リテラル
         *
         * @type {Object}
         */
        msg: {
            error: {
                timeout: 'タイムアウトしました。',
                notfound: '詳細データファイルが見つかりませんでした。'
            },
            string: {
                option: 'オプション',
                hint: {
                    area: 'エリア詳細'
                },
                saved: {
                    title: '保存',
                    body: 'データの保存に成功しました。'
                }
            }
        },
        /**
         * Underscore用htmlテンプレート
         *
         * @type {Object}
         */
        templates: {
            info:
                '<div class="sfa-alert js-sfa-alert alert alert-success alert-dismissible" role="alert">' +
                '    <strong><%= t %></strong><%= b %>' +
                '</div>',
            saved:
                '<div class="sfa-alert js-sfa-alert alert alert-attention alert-dismissible" role="alert">' +
                '    <strong><%= t %></strong><%= b %>' +
                '</div>',
            // キャンペーンボタン
            campaign:
                '<div data-toggle="buttons">' + "\n" +
                '<label class="btn btn-info">' + "\n" +
                '<input type="checkbox" '+
                'name="<%= section %>[campaign][<%= idx %>]" ' +
                'autocomplete="off"> <%= name %>' + "\n" +
                '</label><button class="btn btn-info js-sfa-btn-info" ' +
                'type="button" data-href="<%= file %>">' + "\n" +
                '<span class="glyphicon <%= icon %>"></span>' + "\n" +
                '</button>' + "\n" +
                '</div>' + "\n",
            estimate:
                '<tr>' + "\n" +
                '<td class="sfa-text-align-left"><%= s %></td>' + "\n" +
                '<td class="sfa-text-align-right"><%= n %></td>' + "\n" +
                '<td class="sfa-text-align-right"><%= t %></td>' + "\n" +
                '</tr>' + "\n",
            estimateCampaign:
                '<tr>' + "\n" +
                '<td class="sfa-text-align-left"><%= s %></td>' + "\n" +
                '<td class="sfa-text-align-right"><%= t %></td>' + "\n" +
                '</tr>' + "\n"
        },
        /**
         * init
         *
         * アプリケーション初期化
         */
        init: function () {

            var self = this;

            // json読み込み → DOM生成
            return self.setInterface().done(function () {

                // 全フォームのname属性の再構成
                _.each($('.sfa-article'), function (parent) {
                    self.setNamesValueToHash($(parent));
                });
            });

        },
        /**
         * setInterface
         *
         * jsonpファイルを読み込む
         *
         * @return {Object}
         */
        setInterface: function () {

            var self = this;

            return $.ajax({
                type: 'GET',
                timeout: 3000,
                url: self.interfaceFile,
                dataType: 'jsonp',
                jsonpCallback: 'detail'
            }).done(
                // 成功時
                function (json) {
                    // プロパティに代入
                    self.interface = json;
                    // selectタグ生成
                    self.setSelectOptions(json);
                    // input + button生成
                    self.setCheckboxButtons(json);
                }
            ).fail(
                // 失敗時
                function (e) {
                    switch(e.status) {
                    case 404:
                        alert(self.msg.error.notfound);
                        break;
                    default:
                        alert(self.msg.error.timeout);
                    }
                    console.log(e.status);
                }
            );
        },
        /**
         * setNumberFormat
         *
         * 数値に3桁ごとにカンマを入れる
         *
         * @param  {Number} num
         * @return {String}
         */
        setNumberFormat: function (num) {
            return String(num).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
        },
        /**
         * setSelectOptions
         *
         * jsonデータを元にフォーム（selectタグ）を生成する
         *
         * @param  {Object}  json 読み込んだjsonデータ
         * @return {Boolean} true
         */
        setSelectOptions: function (json) {

            var self = this;

            // イーアイデム
            _.each([
                [
                    json.eaidem.plan,
                    $('select[data-name=eaidem][data-key=plan]')
                ],
                [
                    json.eaidem.span,
                    $('select[data-name=eaidem][data-key=span]')
                ],
                [
                    json.eaidem.option,
                    $('select[data-name=eaidemopt][data-key=option]')
                ],
            ], function (elem) {
                _.each(elem[0], function(key, idx) {
                    var $opt = '<option value="' + idx + '">' +
                            key.name+'</option>';
                    elem[1].append($opt);
                });
            });
            // ジョブアイデム
            _.each([
                [
                    json.jobaidem.size,
                    $('select[data-name=jobaidem][data-key=size]')
                ],
                [
                    json.jobaidem.span,
                    $('select[data-name=jobaidem][data-key=span]')
                ],
                [
                    json.jobaidem.area,
                    $('select[data-name=jobaidem][data-key=area]'),
                ],
            ], function (elem) {
                _.each(elem[0], function(key, idx) {
                    var $opt = '<option value="' + idx + '">' +
                            key.name + '</option>';
                    elem[1].append($opt);
                });
            });

            // しごと情報アイデム
            _.each([
                [
                    json.shigoto.size,
                    $('select[data-name=shigoto][data-key=size]')
                ],
                [
                    json.shigoto.span,
                    $('select[data-name=shigoto][data-key=span]')
                ],
                [
                    json.shigoto.area,
                    $('select[data-name=shigoto][data-key=area]')
                ],
            ], function (elem) {
                _.each(elem[0], function(key, idx) {
                    var $opt = '<option value="' + idx + '">' +
                            key.name + '</option>';
                    elem[1].append($opt);
                });
            });
        },
        /**
         * setCheckboxButtons
         *
         * jsonデータを元にフォーム（input、buttonのセット）を生成する
         *
         * @param  {Object}  json 読み込んだjsonデータ
         * @return {Boolean} true
         */
        setCheckboxButtons: function (json) {

            var self = this;

            // テンプレートにバインド
            var compiled = _.template(self.templates.campaign);
            // DOMに適用
            _.each([
                [
                    json.eaidem.campaign,
                    $('.sfa-eaidem'), 'eaidem'
                ],
                [
                    json.jobaidem.campaign,
                    $('.sfa-jobaidem'), 'jobaidem'
                ],
                [
                    json.shigoto.campaign,
                    $('.sfa-shigoto'), 'shigoto'
                ],
                [
                    json.total.campaign,
                    $('.sfa-total'), 'total'
                ],
            ], function (elem) {
                _.each(elem[0], function(key, idx) {
                    var icon = (key.file) ? 'glyphicon-info-sign' : 'glyphicon-ban-circle';
                    elem[1].find('.js-sfa-input-group').append(compiled({
                        section: elem[2],
                        idx:  idx,
                        name: key.name,
                        file: key.file,
                        icon: icon
                    }));
                });
            });
            // リンクボタンを有効化
            $('.js-sfa-btn-info').on('click', function () {
                var href = $(this).data('href');
                return (href) ? window.open(href, '_blank') : false ;
            });
        },
        /**
         * setFormElementsToBlank
         *
         * 指定されたエレメント内に存在するフォーム要素すべてをリセットする
         *
         * @param  {Object}  $obj jQueryオブジェクト
         * @return {Boolean} $obj
         */
        setFormElementsToBlank: function ($obj) {
            return $obj
                .find('textarea, input, select').val('')
                .removeClass('sfa-valid sfa-invalid').end()
                .find(':checked').prop('checked', false);
        },
        /**
         * setNamesValueToHash
         *
         * 複数要素で1行になっているフォーム要素のname属性を
         * `name[0][key]`のようにインデックス値の入った状態に組みなおす。
         *
         * @param  {Object}  parent .sfa-articleを持つ親となる要素
         * @return {Boolean} true
         */
        setNamesValueToHash: function (parent) {

            // インデックス用カウンター初期化
            var indexCounter = 0;

            // 行のイテレーション
            _.each(parent.children(), function (child) {

                // 各form要素のイテレーション
                _.each($(child).children(), function (elem) {

                    // オブジェクトのキャッシュ生成
                    var $form = $(elem)
                            .find('textarea, select, input');
                    // 既存nameのままなら処理しない
                    if (!$form.data('name') || !$form.data('key')) {
                        return false;
                    }
                    // 各種パラメータ
                    var dataName = $form.data('name'),
                        dataKey = $form.data('key'),
                        name = dataName +
                            '[' + indexCounter + '][' + dataKey + ']';

                    // name要素を書き換え
                    // `data-*``要素を削除
                    $form.attr('name', name)
                        .removeAttr('data-name')
                        .removeAttr('data-key');
                });

                // カウンターをインクリメント
                indexCounter++;

            });

            return true;
        },
        /**
         * setAggregateAll
         *
         * 入力されたデータすべてを集計する
         * 先にバリデーション（Valid.validate()）を行っておく必要がある。
         *
         * @return {Boolean}
         */
        setAggregateAll: function () {

            var self = this,
                ifc = self.interface;

            // 集計データ
            // イベント毎に集計しなおすので毎回クリアする
            self.aggregate = [];
            self.discount = [];

            // 商材プランの集計
            _.each([
                [
                    'eaidem',
                    $('.js-sfa-form', '.js-sfa-eaidem')
                ],
                [
                    'eaidemopt',
                    $('.js-sfa-form', '.js-sfa-eaidemopt')
                ],
                [
                    'jobaidem',
                    $('.js-sfa-form', '.js-sfa-jobaidem')
                ],
                [
                    'shigoto',
                    $('.js-sfa-form', '.js-sfa-shigoto')
                ]
            ], function (elem) {

                var key = elem[0];

                _.each(elem[1], function (parent) {

                    var serviceName,    // 商材名
                        plan, planName, // プラン、プラン名
                        span, spanName, // 期間、期間名
                        size, sizeName, // サイズ、サイズ名
                        area, areaName, // エリア、エリア名,
                        opt, optName,   // オプション、オプション名
                        num, optnum, price, label;

                    switch (key) {
                        case 'eaidemopt':
                        case 'eaidem':
                            serviceName = ifc.eaidem.service.name;
                            plan = $(parent)
                                .find('[name*=eaidem][name*=plan]')
                                .val();
                            span = $(parent)
                                .find('[name*=eaidem][name*=span]')
                                .val();
                            num = $(parent)
                                .find('[name*=eaidem][name*=num]')
                                .val();
                            opt = $(parent)
                                .find('[name*=eaidemopt][name*=option]')
                                .val();
                            optnum = $(parent)
                                .find('[name*=eaidemopt][name*=num]')
                                .val();
                            if(plan && span && num ) {

                                price = ifc.eaidem.plan[plan].price[span];
                                planName = ifc.eaidem.plan[plan].name;
                                spanName = ifc.eaidem.span[span].name;

                                label = serviceName + ' ' +
                                    planName + ' ' + spanName;
                                self.aggregate.push([
                                    label, num, price
                                ]);

                            } else if( opt && optnum ) {

                                price = ifc.eaidem.option[opt].price[1];
                                optName = ifc.eaidem.option[opt].name;

                                // 集計データにまとめる
                                label = self.msg.string.option + ' ' + optName;
                                self.aggregate.push([
                                    label, optnum, price
                                ]);
                            }

                            break;
                        case 'jobaidem':
                        case 'shigoto':
                            serviceName = ifc[key].service.name;
                            size = $(parent)
                                .find('[name*=' + key + '][name*=size]')
                                .val();
                            span = $(parent)
                                .find('[name*=' + key + '][name*=span]')
                                .val();
                            area = $(parent)
                                .find('[name*=' + key + '][name*=area]')
                                .val();
                            num = $(parent)
                                .find('[name*=' + key + '][name*=num]')
                                .val();
                            if(size && span && area && num ) {

                                price = ifc[key].size[size].price[area] * span;
                                sizeName = ifc[key].size[size].name;
                                spanName = ifc[key].span[span].name;
                                areaName = ifc[key].area[area].name;

                                // 集計データにまとめる
                                label = serviceName + ' ' +
                                    sizeName + ' ' + spanName;
                                self.aggregate.push([
                                    label, num, price
                                ]);
                            }
                        break;
                    }
                });
            });

            // 割引キャンペーンの集計
            _.each([
                $('input:checked[type=checkbox][name^=eaidem]'),
                $('input:checked[type=checkbox][name^=jobaidem]'),
                $('input:checked[type=checkbox][name^=shigoto]'),
                $('input:checked[type=checkbox][name^=total]')
            ], function (elem) {

                _.each(elem, function (camp) {
                    var service = $(camp).attr('name'),
                        _t = self.getNames(camp),
                        key = _t.getService(),
                        code = _t.getCode();
                    var discount = ifc[key].campaign[code].discount,
                        discountName = ifc[key].campaign[code].discountName;
                    discountName = (!discountName) ? discount: discountName;

                    var name = ifc[key].campaign[code].name,
                        serviceName = ifc[key].service.name;

                    // 集計データにまとめる
                    var label = serviceName + ' ' + name;
                    self.discount.push([
                        label, discount, discountName
                    ]);

                });

            });

            // 見積もり生成
            self.setEstimate();

            return true;
        },
        /**
         *
         * setEstimate
         *
         * 集計データを見積もりデータに整形する
         *
         * @return {Boolean}
         */
        setEstimate: function () {

            var self = this,
                ifc = self.interface,
                maxAggregate = self.aggregate.length,
                maxDiscount = self.discount.length,
                total, subtotal = 0, tax = 0, i = 0,
                service, num, price, t, compiled;

            // 見積もり前にデータをクリア
            _.each([
                '.js-sfa-estimate-service',
                '.js-sfa-estimate-campaign',
                '.js-sfa-estimate-subtotal',
                '.js-sfa-estimate-tax',
                '.js-sfa-estimate-total'
            ], function (elem) {
                $(elem).empty();
            });

            // 商材プランの見積もり
            for( i=0; i < maxAggregate; i++) {

                service = self.aggregate[i][0];
                num = parseInt(self.aggregate[i][1], 10);
                price = parseInt(self.aggregate[i][2], 10);
                t = price * num;

                // テンプレートにバインド
                compiled = _.template(self.templates.estimate);
                // DOMに適用
                $('.js-sfa-estimate-service').append(compiled({
                    s: self.setNumberFormat(service),
                    n: self.setNumberFormat(num),
                    t: self.setNumberFormat(t)
                }));

                // 合計値に加算
                subtotal += t;
            }

            // 割引キャンペーンの見積もり
            for( i=0; i < maxDiscount; i++) {

                service = self.discount[i][0];
                price =   self.discount[i][1];
                var priceName;
                // 割引名を代入
                if(!self.discount[i][2]) {
                    priceName = price;
                } else {
                    priceName = self.discount[i][2];
                }

                // テンプレートにバインド
                compiled = _.template(self.templates.estimateCampaign);
                // DOMに適用
                $('.js-sfa-estimate-campaign').append(compiled({
                    s: self.setNumberFormat(service),
                    t: self.setNumberFormat(priceName)
                }));

                // 割引額を計算して小計に反映させる
                var re = /^([\/\*\-\+]) ?([0-9\.]+)$/,
                    calc = re.exec(price);
                switch(calc[1]) {
                    case '*':
                        subtotal = subtotal * Number(calc[2]);
                        break;
                    case '/':
                        subtotal = subtotal / Number(calc[2]);
                        break;
                    case '-':
                        subtotal = subtotal - Number(calc[2]);
                        break;
                    case '+':
                        subtotal = subtotal + Number(calc[2]);
                        break;
                }

            }

            tax = Math.round(subtotal * ifc.tax.consumption.rate);
            total = Math.round(subtotal + tax); // 四捨五入する

            // 小計
            $('.js-sfa-estimate-subtotal')
                .append(self.setNumberFormat(subtotal));
            // 消費税
            $('.js-sfa-estimate-tax')
                .append(self.setNumberFormat(tax));
            // 合計
            $('.js-sfa-estimate-total')
                .append(self.setNumberFormat(total));

            return true;
        },
        /**
         * getNames
         *
         * htmlのname属性を商材名、インデックス値、コード名に分断する
         * 例：service[0][code]
         *     → service
         *     → 0
         *     → code
         * に分解
         *
         * それぞれのゲッターメソッドを戻す
         *
         * @param  {String} elem
         * @return {Object}      ゲッターメソッドオブジェクト
         */
        getNames: function (elem) {

            var reg = /^([a-z]+)\[([a-z0-9]+)\]\[([a-z0-9]+)\]$/;
            var name = $(elem).attr('name');
            var service = name.replace(reg, '$1'),
                index = name.replace(reg, '$2'),
                code = name.replace(reg, '$3');

            return {
                getService: function () {
                    return service;
                },
                getIndex: function () {
                    return index;
                },
                getCode: function () {
                    return code;
                },
            };
        },
        /**
         * setHint
         *
         * ヒント表示
         *
         * @param {object} obj DOMオブジェクト
         */
        setHint: function (obj) {
            var self = this,
                p = self.getNames(obj),
                hint;

            var service = p.getService(),
                index   = p.getIndex(),
                code    = p.getCode(),
                selected = $(obj).find('option:selected').val();

            // エリアの場合
            if( selected && code === 'area' ) {
                hint = self.interface[service].hint.area[selected];
                self.setNotice({
                    title: self.msg.string.hint.area,
                    body: hint,
                    delay: 5000,
                    close: true
                });
            }
        },
        /**
         * setNotice
         * 通知領域
         *
         * @param {object} param title, body, delay, close
         */
        setNotice: function (param) {
            var self = this;
            var compiled = _.template(self.templates.info);
            var $stack = $('.js-sfa-notice');
            param.delay = param.delay || 3000;
            var $a = compiled({
                t: param.title,
                b: param.body
            });
            $stack.append($a);
            $stack.find('div:last').delay(5000).animate({
                opacity: 0,
                height: 0,
                margin: 0,
                padding: 0
            }, 500, function () {
                $(this).remove();
            });
        },
        /**
         * getSaveData
         *
         * フォーム内容をJSON形式にして保存する
         *
         * @return null
         */
        getSaveData: function () {
            var self = this;

            // イーアイデムフォーム
            var $eaidem = $('.js-sfa-eaidem').find('.sfa-inline');
            var $eaidemopt = $('.js-sfa-eaidemopt').find('.sfa-inline');
            // ジョブアイデム
            var $jobaidem = $('.js-sfa-jobaidem').find('.sfa-inline');
            // しごと情報アイデム
            var $shigoto = $('.js-sfa-shigoto').find('.sfa-inline');
            // フォームデータ
            var formData = {};

            // 保存用オブジェクトを仕上げる
            _.each([
                [ 'eaidem', $eaidem ],
                [ 'eaidemopt', $eaidemopt ],
                [ 'jobaidem', $jobaidem ],
                [ 'shigoto', $shigoto ]
            ], function (serviceName) {

                // 一時保管用配列
                var _tmp = {};

                _.each(serviceName[1], function (elems) {
                    var row = $(elems).find('select, input');

                    // 一時保管用オブジェクト
                    var __tmp = {}, a = {};
                    // スコープ外変数渡し用
                    var index;

                    _.each($(row), function (el) {
                        var name = el.name,
                            val  = $(el).val();
                        var names = self.getNames(el),
                            service = names.getService(),
                            idx = names.getIndex(),
                            code = names.getCode();
                        // キー名を変数で使うための処理
                        //Object.defineProperty(a, code, { value: val });
                        a[code] = val;

                        // スコープ外利用のためにインデックスを代入
                        index = idx;
                    });
                    // 配列でデータを入れる
                    //Object.defineProperty(_tmp, index, { value: a });
                    _tmp[index] = a;

                });
                // サービス名のプロパティを生成してデータを全部代入
                //Object.defineProperty(formData, serviceName[0], { value: _tmp });
                formData[serviceName[0]] = _tmp;

            });

            // ローカルストレージにタイトル名で保存する
            var dataName = $('.sfa-pane-form').find('.js-sfa-title').val();
            localStorage.setItem(dataName, JSON.stringify(formData));

            self.setNotice({
                title: self.msg.string.saved.title,
                body: self.msg.string.saved.body
            });
        }
    };

    return app;

});
