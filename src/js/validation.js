/**
 * AIDEM SFA Validation
 *
 * @fileoverview require files are below.
 * <ul>
 *     <li>jquery</li>
 *     <li>underscorejs</li>
 *     <li>requirejs</li>
 * </ul>
 *
 * @author    Seiji Ogawa [s_ogawa@aidem.co.jp]
 * @copyright 2016.02 aidem, ink.
 *
 */

/**
 * バリデーションオブジェクト
 *
 * @type    {Object}
 * @return  {Object} valid
 */
define(function () {

    'use strict';

    var valid = {
        /**
         * インバリッド個数
         * @type {int}
         */
        invalidCounter: 0,
        /**
         * msg
         *
         * バリデーションメッセージ
         * @type {Object}
         */
        msg: {
            save: {
                incomplete: '不完全な項目があります。',
                empty: 'データが入力されていません。',
                system: 'システムエラーが発生しました。'
            },
            shigoto: {
                limited: ['岡山', '福岡'],
                integrity: {
                    title: 'しごと情報アイデム',
                    body: 'サイズとエリアの組み合わせが不正です',
                    delay: 3000,
                    close: false
                }
            }
        },
        templates: {
            alert: '<div class="sfa-alert js-sfa-alert alert alert-warning alert-dismissible" role="alert">' +
                   '    <strong><%= t %></strong><%= b %>' +
                   '</div>'
        },
        /**
         * バリデート
         *
         * 未入力項目があれば背景色を変更して目立たせる
         *
         * @return {Boolean}
         */
        validate: function () {

            var self = this;

            // インバリッドカウンターをリセット
            self.invalidCounter = 0;

            // 各項目ごとにバリデーション実行
            self.eaidem();
            self.eaidemopt();
            self.jobaidem();
            self.shigoto();

            return (self.invalidCounter === 0) ? true: false;

        },
        /**
         * イーアイデム
         *
         * @return {Boolean}
         */
        eaidem: function () {

            var self = this,
                localErrors = 0;

            // イーアイデム領域オブジェクト取得
            var $eaidem = $('.js-sfa-eaidem').find('.sfa-inline');

            _.each($eaidem, function (elem) {

                // 各パラメータ取得
                var plan = $(elem).find('select[name*=plan]').val(),
                    span = $(elem).find('select[name*=span]').val(),
                    num =  $(elem).find('input[name*=num]').val();

                // 入力判定
                if ( (plan && span && num) || (!plan && !span && !num) ) {
                    $('.form-control', $(elem))
                        .removeClass('sfa-invalid')
                        .addClass('sfa-valid');
                } else if ( !plan || !span || !num ) {
                    $('.form-control', $(elem))
                        .removeClass('sfa-valid')
                        .addClass('sfa-invalid');
                    localErrors++;
                }

            });

            // オブジェクトを破棄（GC）
            $eaidem = null;
            // 成否判定
            self.invalidCounter += localErrors;
            return (localErrors > 0) ? false : true;
        },
        /**
         * イーアイデムオプション
         *
         * @return {Boolean}
         */
        eaidemopt: function () {

            var self = this,
                localErrors = 0;

            // イーアイデムオプションオブジェクト取得
            var $eaidemopt = $('.js-sfa-eaidemopt').find('.sfa-inline');

            _.each($eaidemopt, function (elem) {

                // 各パラメータ取得
                var option = $(elem).find('select[name*=option]').val(),
                    optnum = $(elem).find('input[name*=optnum]').val();

                // 入力判定
                if ( (option && optnum) || (!option && !optnum) ) {
                    $(elem).find('.form-control')
                        .removeClass('sfa-invalid')
                        .addClass('sfa-valid');
                } else if ( !option || !optnum ) {
                    $(elem).find('.form-control')
                        .removeClass('sfa-valid')
                        .addClass('sfa-invalid');
                    localErrors ++;
                }

            });

            // オブジェクトを破棄（GC）
            $eaidemopt = null;
            // 成否判定
            self.invalidCounter += localErrors;
            return (localErrors > 0) ? false : true;

        },
        /**
         * ジョブアイデム
         *
         * @return {Boolean}
         */
        jobaidem: function () {

            var self = this,
                localErrors = 0;

            // ジョブアイデムオブジェクト取得
            var $jobaidem = $('.js-sfa-jobaidem').find('.sfa-inline');

            _.each($jobaidem, function (elem) {

                // 各パラメータ取得
                var size = $(elem).find('select[name*=size]').val(),
                    span = $(elem).find('select[name*=span]').val(),
                    area = $(elem).find('select[name*=area]').val(),
                    num =  $(elem).find('input[name*=num]').val();

                // 入力判定
                if ( (size && span && area && num) || (!size && !span && !area && !num) ) {
                    $('.form-control', $(elem))
                        .addClass('sfa-valid')
                        .removeClass('sfa-invalid');
                } else if ( !size || !span || !area || !num ) {
                    $('.form-control', $(elem))
                        .addClass('sfa-invalid')
                        .removeClass('sfa-valid');
                    localErrors++;
                }

            });

            // オブジェクトを破棄（GC）
            $jobaidem = null;
            // 成否判定
            self.invalidCounter += localErrors;
            return (localErrors > 0) ? false : true;

        },
        /**
         * しごと情報アイデム
         *
         * @return {Boolean}
         */
        shigoto: function () {

            var self = this,
                localErrors = 0;

            // しごと情報アイデムオブジェクト取得
            var $shigoto = $('.js-sfa-shigoto').find('.sfa-inline');

            _.each($shigoto, function (elem) {

                // 各パラメータ取得
                var size = $(elem).find('select[name*=size]').val(),
                    optText = $(elem).find('select[name*=size] option:selected').text(),
                    span = $(elem).find('select[name*=span]').val(),
                    area = $(elem).find('select[name*=area]').val(),
                    areaText = $(elem).find('select[name*=area] option:selected').text(),
                    num =  $(elem).find('input[name*=num]').val(),
                    re = /^J/,
                    res,
                    errNum = self.invalidCounter;

                // 通常のバリデーション
                if ( (size && span && area && num) || (!size && !span && !area && !num) ) {
                    $('.form-control', $(elem))
                        .addClass('sfa-valid')
                        .removeClass('sfa-invalid');
                } else if ( !size || !span || !area || !num ) {
                    $('.form-control', $(elem))
                        .addClass('sfa-invalid')
                        .removeClass('sfa-valid');
                    localErrors++;
                }

                // エリア判定
                res = re.exec(optText);
                if(area && res) {
                    if(_.indexOf(self.msg.shigoto.limited, areaText) < 0) {
                        $('.form-control', $(elem))
                            .addClass('sfa-invalid')
                            .removeClass('sfa-valid');
                        localErrors++;
                        // アラート表示
                        self.setNotice(self.msg.shigoto.integrity);
                    } else {
                        $('.form-control', $(elem))
                            .addClass('sfa-valid')
                            .removeClass('sfa-invalid');
                    }
                }

            });

            // オブジェクトを破棄（GC）
            $shigoto = null;
            // 成否判定
            self.invalidCounter += localErrors;
            return (localErrors > 0) ? false : true;

        },
        /**
         * beforeSave
         *
         * 保存ボタンクリック時のアクション
         * タイトル未記入は日付時刻をタイトルとする
         *
         * @return {Boolean} [description]
         */
        beforeSave: function () {
            var $form = $('.sfa-pane-form'),
                title = $form.find('.js-sfa-title').val();

            if (title.length === 0) {
                var d = new Date();
                var today = [
                    d.getFullYear(), '.',
                    ('0' + (d.getMonth() + 1)).slice(-2), '.',
                    ('0' + d.getDate()).slice(-2), ' ',
                    ('0' + d.getHours()).slice(-2), ':',
                    ('0' + d.getMinutes()).slice(-2), ':',
                    ('0' + d.getSeconds()).slice(-2)
                ].join('');
                $form.find('.js-sfa-title').val(title + ' ' + today);
            }

            return true;
        },
        /**
         * setNotice
         * 通知領域
         *
         * @param {object} param title, body, delay, close
         */
        setNotice: function (param) {
            var self = this;
            var compiled = _.template(self.templates.alert);
            var $stack = $('.js-sfa-notice');
            param.delay = param.delay || 3000;
            var $a = compiled({
                t: param.title,
                b: param.body
            });
            $stack.append($a);
            $stack.find('div:last').delay(5000).animate({
                opacity: 0,
                height: 0,
                margin: 0,
                padding: 0
            }, 500, function () {
                $(this).remove();
            });
        }
    };

    return valid;

});
