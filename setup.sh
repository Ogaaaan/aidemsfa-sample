# Provisioning ShellScript [bash]
#
# @author    Seiji Ogawa [@ogaaaan]
# @license   MIT license
# @copyright ogaaaan

LANG=C
CERTS_DIR="/etc/pki/tls/certs"
PW="vagrant"
YUM_REPOSD="/etc/yum.repos.d"
LOCAL_TIME="Japan"
LOCALE_NAME="ja_JP.UTF-8"

# Change variables below as you wish.
#

# Apache
SERVER_NAME="example.local"
# php
MEMORY_LIMIT="512M"
POST_MAX_SIZE="512M"
UPLOAD_MAX_FILESIZE="512M"
# mysql
LONG_QUERY_TIME="3"

###


# STOP iptables
#
echo -e "\n[ STOP iptables ]\n"
# ipv4
service iptables stop
chkconfig iptables off
# ipv6
service ip6tables stop
chkconfig ip6tables off


# STOP SELinux
#
echo -e "\n[ STOP SELinux ]\n"
sed -i -e "/^SELINUX=enforcing$/c\SELINUX=disabled" /etc/selinux/config
setenforce 0


# SET i18n Japanese
#
echo -e "\n[ SET i18n Japanese ]\n"
sed -i -e "/^LANG=/c\LANG=\"${LOCALE_NAME}\"" /etc/sysconfig/i18n


# Change Timezone
#
echo -e "\n[ SET timezone JST ]\n"
cp /usr/share/zoneinfo/${LOCAL_TIME} /etc/localtime


# UPDATE system
#
echo -e "\n[ UPDATE System ]\n"
yum -y update


# Kernel Update
#
echo -e "\n[ UPDATE kernel ]\n"
yum -y update kernel
yum -y install kernel-devel kernel-headers dkms gcc gcc-c++


# INSTALL utils
#
echo -e "\n[ INSTALL utils ]\n"
yum -y groupinstall 'Development Tools'
rpm -qa expect | grep "expect"
if [ ! $? -eq 0 ]; then
    yum -y install expect bind-utils wget vim rsync make jq p7zip p7zip-plugins
else
    echo -e "[ SKIP ] already installed.\n"
fi

# SET vim environment
#
echo -e "\n[ SET vim conf ]\n"
grep -q 'vim -p' /home/vagrant/.bashrc
if [ ! $? -eq 0 ]; then
    echo "alias vi='vim -p'" >> /home/vagrant/.bashrc
fi
sudo grep -q "vim -p" /root/.bashrc
if [ ! $? -eq 0 ]; then
    sudo sed -i -e "/mv='mv -i'/a alias vi='vim -p'" /root/.bashrc
    source /root/.bashrc
fi
# install NeoBundle
sudo -u vagrant -i env mkdir -p /home/vagrant/.vim/bundle/
sudo -u vagrant -i env git clone git://github.com/Shougo/neobundle.vim /home/vagrant/.vim/bundle/neobundle.vim


# ADD repositories
#
echo -e "\n[ ADD epel repository ]\n"
if [ ! -e ${YUM_REPOSD}/epel.repo ]; then
    yum -y install epel-release
else
    echo -e "[ SKIP ] epel exists.\n"
fi
echo -e "\n[ ADD remi repository ]\n"
if [ ! -e ${YUM_REPOSD}/remi.repo ]; then
    rpm -ivh http://fr2.rpmfind.net/linux/remi/enterprise/6.7/remi/x86_64/remi-release-6.6-2.el6.remi.noarch.rpm
else
    echo -e "[ SKIP ] remi exists.\n"
fi
echo -e "\n[ ADD city-fan repository ]\n"
if [ ! -e ${YUM_REPOSD}/city-fan.org.repo ]; then
    rpm -Uvh http://www.city-fan.org/ftp/contrib/yum-repo/city-fan.org-release-1-13.rhel6.noarch.rpm
    sed -i -e "s/^enabled=1/enabled=0/" /etc/yum.repos.d/city-fan.org.repo
    yum -y update --enablerepo=city-fan.org libcurl
else
    echo -e "[ SKIP ] city-fan exist.\n"
fi


# INSTALL Apache
#
echo -e "\n[ INSTALL Apache ]\n"
rpm -qa httpd | grep "httpd"
if [ ! $? -eq 0 ]; then
    yum -y install httpd mod_ssl mod_perl mod_rewrite
    echo -e "\n[ CONFIG Apache ]\n"
    sed -i -e "/^#ServerName/c\ServerName ${SERVER_NAME}:80" /etc/httpd/conf/httpd.conf
    sed -i -e "/^#EnableSendfile/c\EnableSendfile Off" /etc/httpd/conf/httpd.conf
    sed -i -e "s/Options FollowSymLinks/Options Includes ExecCGI FollowSymLinks/g" /etc/httpd/conf/httpd.conf
    sed -i -e "s/AllowOverride None/AllowOverride All/g" /etc/httpd/conf/httpd.conf
    sed -i -e "/^#NameVirtualHost/c\NameVirtualHost *:80" /etc/httpd/conf/httpd.conf
    grep -e '^<VirtualHost' /etc/httpd/conf/httpd.conf
    if [ ! $? -eq 0 ]; then
        echo -e "<VirtualHost *:80>\n    ServerAdmin  root@localhost\n    ServerName   ${SERVER_NAME}:80\n    DocumentRoot /var/www/html\n</VirtualHost>\n" >> /etc/httpd/conf/httpd.conf
    fi
else
    echo -e "[ SKIP ] already installed.\n"
fi


# INSTALL php
#
echo -e "\n[ INSTALL php ]\n"
rpm -qa php | grep "php"
if [ ! $? -eq 0 ]; then
    yum -y --enablerepo=remi --enablerepo=remi-php56 install \
        php php-devel php-bcmath php-gd php-mbstring php-mcrypt \
        php-mysql php-pdo php-pear php-xml php-xmlrpc php-intl
    echo -e "\n[ CONFIG php ]\n"
    sed -i -e "/^memory_limit/c\memory_limit = ${MEMORY_LIMIT}" /etc/php.ini
    sed -i -e "/^post_max_size/c\post_max_size = ${POST_MAX_SIZE}" /etc/php.ini
    sed -i -e "/^upload_max_filesize/c\upload_max_filesize = ${UPLOAD_MAX_FILESIZE}" /etc/php.ini
    sed -i -e "/^;date.timezone/c\date.timezone = Asia/Tokyo" /etc/php.ini
    sed -i -e "/^;mbstring.language/c\mbstring.language = Japanese" /etc/php.ini
    sed -i -e "/^;mbstring.internal_encoding/c\mbstring.internal_encoding = UTF-8" /etc/php.ini
    sed -i -e "/^;mbstring.http_input/c\mbstring.http_input = UTF-8" /etc/php.ini
    sed -i -e "/^;mbstring.http_output/c\mbstring.http_output = UTF-8" /etc/php.ini
    sed -i -e "/^;mbstring.detect_order/c\mbstring.detect_order = auto" /etc/php.ini
else
    echo -e "[ SKIP ] already installed.\n"
fi

# Clear Cache
#
yum clean all


# INSTALL MySQL
#
echo -e "\n[ INSTALL mysql ]\n"
rpm -qa mysql | grep "mysql"
if [ ! $? -eq 0 ]; then
    yum -y --enablerepo=remi install mysql-server
    echo -e "\n[ CONFIG mysql ]\n"
    touch /var/log/mysqld_slow.log
    chown mysql:mysql /var/log/mysqld_slow.log
    grep -e '^default-storage-engine=INNODB' /etc/my.cnf
    if [ ! $? -eq 0 ]; then
        sed -i -e "/^symbolic-links=0$/a default-storage-engine=INNODB" /etc/my.cnf
        sed -i -e "/^default-storage-engine=INNODB$/a skip-character-set-client-handshake" /etc/my.cnf
        sed -i -e "/^skip-character-set-client-handshake$/a character-set-server=utf8" /etc/my.cnf
        sed -i -e "/^character-set-server=utf8$/a innodb_file_per_table" /etc/my.cnf
        sed -i -e "/^innodb_file_per_table$/a innodb_file_format=Barracuda" /etc/my.cnf
        sed -i -e "/^innodb_file_format=Barracuda$/a innodb_file_format_max=Barracuda" /etc/my.cnf
        sed -i -e "/^user=mysql$/a slow_query_log=ON" /etc/my.cnf
        sed -i -e "/^slow_query_log=ON$/a slow_query_log_file=/var/log/mysqld_slow.log" /etc/my.cnf
        sed -i -e "/^slow_query_log_file/a long_query_time=${LONG_QUERY_TIME}" /etc/my.cnf
        mysql_install_db
    fi
    # CONFIG MySQL initialize
    #
    echo -e "\n[ CONFIG mysql Initialize ]\n"
    expect -c "
    set timeout 3
    spawn env LANG=C sudo mysql_secure_installation
    expect \"(enter for none):\"
    send -- \"yes\n\"
    expect \"Set root password\? \[Y\/n\]\"
    send -- \"\n\"
    expect \"Change the root password\? \[Y\/n\]\"
    send -- \"\n\"
    expect \"New password:\"
    send -- \"${PW}\n\"
    expect \"Re\-enter new password:\"
    send -- \"${PW}\n\"
    expect \"Remove anonymous users\? \[Y\/n\]\"
    send -- \"\n\"
    expect \"Disallow root login remotely\? \[Y\/n\]\"
    send -- \"\n\"
    expect \"Remove test database and access to it\? \[Y\/n\]\"
    send -- \"\n\"
    expect \"Reload privilege tables now\? \[Y\/n\]\"
    send -- \"\n\"
    expect \"Thanks for using MySQL!\"
    exit 0
    "
    # add MySQL User
    #
    echo -e "\n[ ADD mysql user ]\n"
    expect -c "
    set timeout 3
    spawn mysql -uroot -p
    expect \"Enter password:\"
    send -- \"${PW}\n\"
    expect \"mysql\>\"
    send -- \"grant all privileges on \*\.\* to vagrant\@localhost identified by \'${PW}\';\n\"
    expect \"mysql\>\"
    send -- \"grant all privileges on \*\.\* to vagrant\@\\\"%\\\" identified by \'${PW}\';\n\"
    expect \"mysql\>\"
    send -- \"flush privileges;\n\"
    expect \"mysql\>\"
    send -- \"exit\n\"
    expect \"Bye\"
    exit 0
    "
else
    echo -e "[ SKIP ] already installed.\n"
fi


# Symbolic-links
#
echo -e "\n[ LINK document root ]\n"
rm -Rf /var/www/html
ln -s /home/vagrant/htdocs /var/www/html
chmod 755 /home/vagrant


# START Servers
#
echo -e "\n[ START httpd, mysqld ]\n"
ps -ef | grep httpd | grep -v grep | wc -l
if [ $? -eq 0 ]; then
    service httpd start
    chkconfig httpd on
fi
ps -ef | grep mysql | grep -v grep | wc -l
if [ $? -eq 0 ]; then
    service mysqld start
    chkconfig mysqld on
fi


# Nodebrew
#
echo -e "\n[ INSTALL Nodebrew ]\n"
sudo -u vagrant -i env nodebrew -v | grep "Usage"
if [ $? -eq 1 ]; then
    curl -L git.io/nodebrew | sudo -u vagrant -i env perl - setup
    grep -e '.nodebrew' /home/vagrant/.bash_profile
    if [ ! $? -eq 0 ]; then
        sudo -u vagrant -i env sed -i -e "/^export PATH$/c\export PATH=\$HOME/.nodebrew/current/bin:\$PATH" /home/vagrant/.bash_profile
    fi
    source /home/vagrant/.bash_profile
    # NodeJS
    #
    echo -e "\n[ INSTALL NodeJS ]\n"
    sudo -u vagrant -i env nodebrew install-binary v4.1.2
    sudo -u vagrant -i env nodebrew install-binary v4.2.2
    sudo -u vagrant -i env nodebrew use v4.2.2
else
    echo -e "[ SKIP ] already installed.\n"
fi

# RVM
#
echo -e "\n[ INSTALL RVM ]\n"
sudo -u vagrant -i env rvm -v | grep "Seguin"
if [ $? -eq 1 ]; then
    curl -sSL https://rvm.io/mpapis.asc | sudo -u vagrant -i env gpg2 --import -
    curl -L https://get.rvm.io | sudo -u vagrant -i env bash -s stable
    source /home/vagrant/.profile
    # Ruby
    #
    echo -e "\n[ INSTALL Ruby ]\n"
    sudo -u vagrant -i env rvm install --autolibs=packages 2.2.1
    sudo -u vagrant -i env rvm alias create default 2.2.1
else
    echo -e "[ SKIP ] already installed.\n"
fi


# Compass
#
echo -e "\n[ INSTALL Compass ]\n"
sudo -u vagrant -i env compass -v | grep "Eppstein"
if [ $? -eq 1 ]; then
    sudo -u vagrant -i env gem update --system
    sudo -u vagrant -i env gem install compass
else
    echo -e "[ SKIP ] already installed.\n"
fi


# Gulp
#
echo -e "\n[ INSTALL Gulp to global & local ]\n"
sudo -u vagrant -i env gulp -v | grep "CLI version"
if [ $? -eq 1 ]; then
    sudo -u vagrant -i env npm install -g gulp
    cd /home/vagrant/htdocs
    sudo -u vagrant -i env npm install gulp --no-bin-links
else
    echo -e "[ SKIP ] already installed.\n"
fi


# DONE
#
echo -e "\n[ PROVISION DONE ]\n"
echo -e "!!!PLEASE RELOAD THIS SERVER!!!\n\n"
echo -e "   > vagrant reload \n\n"
